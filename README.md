# miscellaneous

Various scripts and files


## files

1. [**diagrams/time-keeping-script/**](https://salsa.debian.org/lts-team/packages/miscellaneous/-/blob/main/diagrams/time-keeping-script/) -- PlantUML diagram for the time-keeping-script

1. [**stretch/.gitlab-ci.yml**](https://salsa.debian.org/lts-team/packages/miscellaneous/-/blob/main/stretch/.gitlab-ci.yml) -- gitlab-CI file for the stretchscellaneous/-/raw/main/stretch/.gitlab-ci.yml -P debian

1. [**diagrams/upload-security-fix-process/**](https://salsa.debian.org/lts-team/packages/miscellaneous/-/blob/main/diagrams/upload-security-fix-process/) -- PlantUML diagram for the process of preparing the security fix and upload

1. [**diagrams/split-scripts/**](https://salsa.debian.org/lts-team/packages/miscellaneous/-/blob/main/diagrams/upload-security-fix-process/) -- PlantUML diagram for the process of preparing the security fix and upload. [Issue](https://gitlab.com/freexian/services/deblts/-/issues/45)